(define (square a)
  (* a a))

(define (squares-sum a b)
  (+ (square a) (square b)))

(define (largest-squares-sum a b c)
  (if (> a b)
      (if (> b c)
          (squares-sum a b)
          (squares-sum a c))
      (if (> a c)
          (squares-sum a b)
          (squares-sum b c))))
